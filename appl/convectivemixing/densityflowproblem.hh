// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
#ifndef DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH
#define DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/freeflow/navierstokes/problem.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include "co2tables.hh"
#include <dumux/material/fluidsystems/brineco2.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

namespace Dumux
{
template <class TypeTag>
class DensityDrivenFlowProblem;

namespace Properties
{

namespace TTag {
#if !NONISOTHERMAL
struct DensityDrivenFlowProblem { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };
#else
struct DensityDrivenFlowProblem { using InheritsFrom = std::tuple<NavierStokesNCNI, StaggeredFreeFlowModel>; };
#endif
}

// Specialize the fluid system type for this type tag
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using BrineCO2 = FluidSystems::BrineCO2<Scalar,
                                            HeterogeneousCO2Tables::CO2Tables,
                                            Components::TabulatedComponent<Components::H2O<Scalar>>,
                                            FluidSystems::BrineCO2DefaultPolicy</*constantSalinity=*/true, /*simpleButFast=*/true>>;
    static constexpr int phaseIdx = BrineCO2::liquidPhaseIdx;
    using type = FluidSystems::OnePAdapter<BrineCO2, phaseIdx>;
};

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr auto value = 0; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DensityDrivenFlowProblem>
{
#if THREED
    static constexpr auto dim = 3;
#else
    static constexpr auto dim = 2;
#endif
    using type = Dune::YaspGrid<dim>;
};

// Set the grid type
template<class TypeTag>
struct Problem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using type = Dumux::DensityDrivenFlowProblem<TypeTag>;
};

}

/*!
 * \brief Application where CO2 dissolves into water and creates density driven flow
   \todo doc me!
 */
template <class TypeTag>
class DensityDrivenFlowProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr auto transportCompIdx = Indices::conti0EqIdx + 1;
    static constexpr auto transportEqIdx = Indices::conti0EqIdx + 1;

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using SourceValues = GetPropType<TypeTag, Properties::NumEqVector>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

public:
    DensityDrivenFlowProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6), storage_(0.0)
    {
        useWholeLength_ = getParam<bool>("Problem.UseWholeLength");
        xTop_ = getParam<Scalar>("Problem.MoleFractionCO2Top");
        henryCoefficient_ = getParam<Scalar>("Problem.HenryCoefficient");
        temperatureGradient_ = getParam<Scalar>("Problem.TemperatureGradient", 0.0);
        temperatureInitial_ = getParam<Scalar>("Problem.Temperature");

        FluidSystem::init();
        deltaRho_.resize(this->fvGridGeometry().numCellCenterDofs());

        fileTotalMass_.open("totalMassCO2.log", std::ios::app);
        fileTotalMass_<< "time totalMassCO2 totalMass" << std::endl;
        fileTotalMass_.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + temperatureInitial_; } // 10C

//     /*!
//      * \brief Return the sources within the domain.
//      *
//      * \param globalPos The global position
//      */
//     SourceValues sourceAtPos(const GlobalPosition &globalPos) const
//     {
//         return SourceValues(0.0);
//     }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set Dirichlet values for the velocity everywhere
        values.setDirichlet(Indices::velocityXIdx);
        values.setDirichlet(Indices::velocityYIdx);

#if THREED
       values.setDirichlet(Indices::velocityZIdx);
#endif

#if NONISOTHERMAL
        values.setDirichlet(Indices::energyEqIdx);
#endif
        values.setNeumann(Indices::conti0EqIdx);
        values.setNeumann(transportEqIdx);

        if(globalPos[dimWorld-1] > this->fvGridGeometry().bBoxMax()[dimWorld-1] - eps_)
        {
            if(useWholeLength_)
                values.setDirichlet(transportEqIdx);
            else
                if(globalPos[0] > 0.4 && globalPos[0] < 0.6)
                    values.setDirichlet(transportEqIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initialAtPos(globalPos);
        values[transportCompIdx] = xTop_;
        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        // set a fixed pressure in one cell
        return (isLowerLeftCell_(scv) && pvIdx == Indices::pressureIdx);
    }
    
    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
 //   using ParentType::source;
 //   template<class FVElementGeometry, class ElementVolumeVariables, class ElementFaceVariables, class SubControlVolumeFace>
 //   SourceValues source(const Element &element,
 //                       const FVElementGeometry& fvGeometry,
 //                       const ElementVolumeVariables& elemVolVars,
 //                       const ElementFaceVariables& elemFaceVars,
 //                       const SubControlVolumeFace &scvf) const
 //   {
 //       auto source = SourceValues(0.0);

 //        static const Scalar height = getParam<Scalar>("Problem.Height");
 //        static const Scalar factor = getParam<Scalar>("Problem.PseudoWallFractionFactor", 8.0);
 //         source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height, factor);

 //       return source;
 //   }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[Indices::pressureIdx] = 1.1e+5;
        values[transportCompIdx] = 2.5e-7;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;
#if NONISOTHERMAL
        values[Indices::temperatureIdx] = 283.01 + temperatureGradient_ * globalPos[1];
#endif
        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    Scalar time() const
    {
        return timeLoop_->time();
    }

    void calculateTotalCO2Mass(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        Scalar totalMassCO2 = 0.0;
        Scalar totalMass = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bind(element);
                for (auto&& scv : scvs(fvGeometry))
                {
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bind(element, fvGeometry, sol);

                    const auto& volVars = elemVolVars[scv];
                    totalMassCO2 += volVars.density() * volVars.moleFraction(0, 1) * scv.volume();
                    std::cout << "volVars.density = " << volVars.density() << "\n";
                    totalMass += volVars.density() * scv.volume();
                }
        }

        fileTotalMass_.open("totalMassCO2.log", std::ios::app);
        fileTotalMass_<< std::scientific << time() << " " << totalMassCO2 << " " << totalMass << std::endl;
        fileTotalMass_.close();
    }
    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void calculateDeltaRho(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                deltaRho_[ccDofIdx] = elemVolVars[scv].density() - 999.694;
            }
        }
    }

    auto& getDeltaRho() const
    { return deltaRho_; }


    template<class SubControlVolume>
    bool isLowerLeftCell_(const SubControlVolume& scv) const
    { return scv.dofIndex() == 0; }

    // \}

private:
    const Scalar eps_;
    bool useWholeLength_;
    Scalar xTop_;
    std::vector<Scalar> deltaRho_;
    Scalar henryCoefficient_;
    TimeLoopPtr timeLoop_;
    Scalar storage_;
    Scalar temperatureGradient_;
    Scalar temperatureInitial_;
    std::ofstream fileTotalMass_;

};
} //end namespace

#endif
